package com.almaz.recruitment.garage;

import org.junit.Test;

import static com.almaz.recruitment.garage.Car.GERMAN_CAR;
import static com.almaz.recruitment.garage.Car.REGULAR_CAR;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 */
public class GarageTest {

    @Test
    public void testSimpleGarage() {

        Garage g = new SimpleGarage();

        assertEquals(0, g.schedule(REGULAR_CAR, Job.CHECKUP));
        assertEquals(0, g.schedule(REGULAR_CAR, Job.TROUBLESHOOT));
        assertEquals(0, g.schedule(REGULAR_CAR, Job.SERVICE));
    }

    @Test
    public void testSimpleGarage_PaintDay() {

        Garage g = new SimpleGarage();

        assertEquals(0, g.schedule(REGULAR_CAR, Job.CHECKUP));
        assertEquals(2, g.schedule(REGULAR_CAR, Job.PAINT));
    }

    @Test
    public void testSimpleGarageGermanCar() {

        Garage g = new SimpleGarage();

        assertEquals(0, g.schedule(GERMAN_CAR, Job.CHECKUP));
        assertEquals(0, g.schedule(GERMAN_CAR, Job.TROUBLESHOOT));
        assertEquals(0, g.schedule(GERMAN_CAR, Job.SERVICE));
    }

    @Test
    public void testSimpleGarageSeveralCar() {

        Garage g = new SimpleGarage();

        assertEquals(0, g.schedule(GERMAN_CAR, Job.CHECKUP));
        assertEquals(0, g.schedule(REGULAR_CAR, Job.TROUBLESHOOT));
        assertEquals(0, g.schedule(GERMAN_CAR, Job.SERVICE));
    }

    @Test
    public void testPaintJob() {

        Garage g = new SimpleGarage();

        assertEquals(2, g.schedule(REGULAR_CAR, Job.PAINT));
    }

    @Test
    public void testPaintJobGermanCar() {

        Garage g = new SimpleGarage();

        assertEquals(2, g.schedule(GERMAN_CAR, Job.PAINT));
    }

    @Test
    public void testRegularGarage() {

        Garage g = new RegularGarage(8);

        assertEquals(0, g.schedule(REGULAR_CAR, Job.SERVICE));
        assertEquals(0, g.schedule(REGULAR_CAR, Job.TROUBLESHOOT));
        assertEquals(0, g.schedule(REGULAR_CAR, Job.SERVICE));

        assertEquals(1, g.schedule(REGULAR_CAR, Job.TROUBLESHOOT));
        assertEquals(3, g.schedule(REGULAR_CAR, Job.PAINT));
        assertEquals(2, g.schedule(REGULAR_CAR, Job.CHECKUP));
    }

    @Test
    public void testRegularGarageNormalHour() {

        Garage g = new RegularGarage(8);

        assertEquals(0, g.schedule(REGULAR_CAR, Job.SERVICE));
        assertEquals(0, g.schedule(REGULAR_CAR, Job.TROUBLESHOOT));
        assertEquals(0, g.schedule(REGULAR_CAR, Job.SERVICE));
    }


    @Test
    public void testGermanCar() {

        Garage g = new RegularGarage(8);

        assertEquals(0, g.schedule(GERMAN_CAR, Job.SERVICE));
        assertEquals(1, g.schedule(GERMAN_CAR, Job.CHECKUP));
        assertEquals(1, g.schedule(REGULAR_CAR, Job.SERVICE));
    }

    /**
     * Bob has documented how a {@link LargeGarage} works, but has not had time to write a proper test case. Please
     * be a boy scout and write one yourself.
     */
//    @Test
    public void testLargeGarage_() {
        fail();
    }

    @Test
    public void testLargeGarageOnePipelinesWithOneCar() {

        Garage g = new LargeGarage(6);

        assertEquals(0, g.schedule(GERMAN_CAR, Job.CHECKUP));//6
    }

    @Test
    public void testLargeGarageMorePipelinesWithTwoCar() {

        Garage g = new LargeGarage(6);

        assertEquals(0, g.schedule(GERMAN_CAR, Job.CHECKUP));//6
        assertEquals(0, g.schedule(GERMAN_CAR, Job.CHECKUP));//6
    }

    @Test
    public void testLargeGarageMorePipelinesWithTwoCarOk() {

        Garage g = new LargeGarage(4);

        assertEquals(1, g.schedule(GERMAN_CAR, Job.CHECKUP));//6
        assertEquals(1, g.schedule(GERMAN_CAR, Job.CHECKUP));//6
    }

    @Test
    public void testLargeGarageMorePipelinesWithTwoCarDifferentService() {

        Garage g = new LargeGarage(4);

        assertEquals(1, g.schedule(GERMAN_CAR, Job.CHECKUP));//6
        assertEquals(1, g.schedule(GERMAN_CAR, Job.TROUBLESHOOT));//8
    }

    @Test
    public void testLargeGarageMorePipelinesWithTwoCarPaint() {

        Garage g = new LargeGarage(4);

        assertEquals(1, g.schedule(GERMAN_CAR, Job.CHECKUP));//6
        assertEquals(3, g.schedule(GERMAN_CAR, Job.PAINT));//2
    }

    @Test
    public void testLargeGaragePaint() {

        Garage g = new LargeGarage(6);

        assertEquals(0, g.schedule(REGULAR_CAR, Job.CHECKUP));//3
        assertEquals(0, g.schedule(REGULAR_CAR, Job.CHECKUP));//3
        assertEquals(3, g.schedule(GERMAN_CAR, Job.PAINT));//2
    }

}
