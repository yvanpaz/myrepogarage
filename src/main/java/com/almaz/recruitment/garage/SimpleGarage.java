package com.almaz.recruitment.garage;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 * This simple garage has no capacity or working hour constraints, it can work on any number of cars at the same time.
 */
public class SimpleGarage implements Garage {

    private OperationGarage operation;

    public SimpleGarage() {
        this.operation = new OperationGarage();
    }

    @Override
    public int schedule(Car car, Job job) {
        return this.operation.getSimpleJob(car, job);
    }
}