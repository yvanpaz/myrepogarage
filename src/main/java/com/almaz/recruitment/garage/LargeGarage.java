package com.almaz.recruitment.garage;

import java.util.ArrayList;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 * The large garage has a limited number of working hours per day. Compared to other garages,
 * it has several pipelines, allowing it to work on several cars at the same time.
 *
 * Once a job is started, the job cannot switch pipelines. The less busy pipeline will be chosen.
 */
public class LargeGarage implements Garage {
    private final OperationGarage operation;
    private final int hours;
    private final ArrayList<int[]> dataTotal;

    public LargeGarage(int hours) {
        this.operation = new OperationGarage();
        this.hours = hours;
        this.dataTotal = new ArrayList<>();
    }

    @Override
    public int schedule(Car car, Job job) {
        int[] dataPipeline = new int[2];
        if (Car.GERMAN_CAR == car) {
            dataPipeline[0] = operation.getHourJobGerman_Car(job);
        } else {
            dataPipeline[0] = operation.getHourJobRegular_Car(job);
        }
        //not busy at start
        dataPipeline[1] = 0;
        this.dataTotal.add(dataPipeline);

        return operation.getLargeJob(job, this.hours, this.dataTotal);
    }

}
