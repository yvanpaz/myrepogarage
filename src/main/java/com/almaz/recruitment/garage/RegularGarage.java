package com.almaz.recruitment.garage;

import java.util.ArrayList;
import java.util.List;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 * The regular garage can work on one car at a time, and has a limited number of working hours per day.
 */
public class RegularGarage implements Garage {

    private final OperationGarage operation;
    private final Boolean flagCar;
    private int daysToWork;
    private final List<String> ctrlCar;
    private final List<Integer> ctrlHours;
    private final int[] data;

    public RegularGarage(int hours) {
        this.operation = new OperationGarage();
        this.daysToWork = 0;
        this.flagCar = true;
        this.ctrlCar = new ArrayList<>();
        this.ctrlHours= new ArrayList<>();
        this.data = new int[3];
        this.data[1] = hours;
    }

    @Override
    public int schedule(Car car, Job job) {

        if(flagCar){
            ctrlCar.add(car.name());
        }

        if(ctrlCar.contains(car.name())){
            if(Car.GERMAN_CAR == car)
                this.ctrlHours.add(operation.getHourJobGerman_Car(job));
            else
                this.ctrlHours.add(operation.getHourJobRegular_Car(job));
            this.daysToWork = operation.getRegularJob(job, this.ctrlHours, this.data);
        }
        return this.daysToWork;
    }



}
