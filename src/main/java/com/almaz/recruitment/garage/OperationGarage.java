package com.almaz.recruitment.garage;

import java.util.ArrayList;
import java.util.List;

public class OperationGarage {

    protected int getSimpleJob(Car car, Job job) {
        if (Car.REGULAR_CAR == car || Car.GERMAN_CAR == car) {
            if (Job.CHECKUP == job
                    || Job.TROUBLESHOOT == job
                    || Job.SERVICE == job) {
                return 0;
            }
            if (Job.PAINT == job) {
                return 2;
            }
        }

        return 0;
    }

    protected int getHourJobRegular_Car(Job job) {
        if (Job.SERVICE == job) {
            return 2;
        } else if (Job.CHECKUP == job) {
            return 3;
        } else if (Job.TROUBLESHOOT == job) {
            return 4;
        } else {
            return 2;
        }
    }

    protected int getHourJobGerman_Car(Job job) {
        if (Job.SERVICE == job) {
            return 4;
        } else if (Job.CHECKUP == job) {
            return 6;
        } else if (Job.TROUBLESHOOT == job) {
            return 8;
        } else {
            return 2;
        }
    }

    protected int getRegularJob(Job job, List<Integer> ctrlHours, int[] data) {
        //data[if you spend the hour of the day, hour per day, daysTotals]
        int hourJob = ctrlHours.get(ctrlHours.size() - 1);
        if (data[1] >= 0 && data[0] != 1) {
            if (data[1] - hourJob == 0) {
                data[0] = 1;
                data[1] = data[1] - hourJob;
            } else {
                if (data[1] - hourJob < 0) {
                    data[1] = 8 - (data[1] - hourJob);
                    data[2] = data[2] + 1;
                } else {
                    data[1] = data[1] - hourJob;
                }
            }
        } else {
            if (data[1] + hourJob > 8) {
                data[1] = data[1] + hourJob;
                data[2] = data[2] + 1;
            } else {
                data[1] = data[1] + hourJob;
                if (Job.PAINT != job) {
                    data[2] = data[2] + 1;
                }
            }
        }
        return Job.PAINT == job ? 3 : data[2];
    }

    protected int getLargeJob(Job job, int hours, ArrayList<int[]> dataTotal) {
        int days = 0;
        int positionPipeline = 0;

        if(dataTotal.size() > 1){
            for(int i=0; i <= dataTotal.size()-1; i++){
                int[] dataDif = dataTotal.get(i);
                if(dataDif[0] >= hours)
                    positionPipeline = i;
            }
        }

        while (true) {
            int[] dataT = dataTotal.get(positionPipeline);
            if(Job.PAINT == job){
                if(days==3){
                    dataT[1] = 0;
                    break;
                }else{
                    dataT[0] = (hours + (dataT[0] - hours));
                    days++;
                }
                dataT[1] = 1;
            }else{
                if((dataT[0] - hours) > 0){
                    dataT[0] = dataT[0] - hours;
                    dataT[1] = 1;
                    days++;
                }else{
                    if((dataT[0] - hours) <= 0) {
                        dataT[0] = (hours + (dataT[0] - hours));
                        dataT[1] = 0;
                        break;
                    }
                }
            }
        }
        return days;
    }
}
